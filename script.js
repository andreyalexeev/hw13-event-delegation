"use scrict";

const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', function (event) {
    if (event.target.classList.contains('tabs-title')) {
        const selectedTab = event.target;
        const selectedTabIndex = Array.from(tabs.children).indexOf(selectedTab);

        const contentItems = tabsContent.querySelectorAll('li');
        contentItems.forEach((item, index) => {
            item.style.display = index === selectedTabIndex ? 'block' : 'none';
            });

        const allTabs = tabs.querySelectorAll('.tabs-title');
        allTabs.forEach(tab => {
            tab.classList.remove('active');
            });
    selectedTab.classList.add('active');
    }
});